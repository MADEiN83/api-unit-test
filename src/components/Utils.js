var fetch = require('node-fetch');
let caching = [];

// DEBUG ONLY
// caching['https://apigreenplanete.maximepaloulack.fr/v1/garbage/type'] = {
//     status: 403,
//     statusText: 'Forbidden',
//     json: {
//         success: false,
//         message: 'No token provided.'
//     }
// }

module.exports = class Utils {
    static getJsonKeyToAnalyse(key, json) {
        let jsonKeyToAnalyse = json

        for(let t of key.split('.')) {
            if(t == '') {
                jsonKeyToAnalyse = jsonKeyToAnalyse[0];
            } else {
                jsonKeyToAnalyse = jsonKeyToAnalyse[t];
            }
        }

        return jsonKeyToAnalyse;
    }
    static setData(data) {
        if(!global.data) {
            global.data = [];
        }
        
        global.data[data.id] = data;
    }

    static getData(id) {
        return global.data[id];
    }

    static buildFetchOptions(data) {
        const headers = data.header != undefined ? data.header : {};
        headers['Content-Type'] = 'application/json';

        const options = { 
            agent: data.proxy,
            method: data.method,
            body: JSON.stringify(data.formData),
            headers: headers
        };
        
        return options;
    }

    static fromCache(data, callback) {
        const { url, noCache } = data;

        if(noCache) {
            data.noCache = false;
            this.setData(data);
            return false;
        }

        const exists = caching[url] != undefined;
        if(exists) {
            callback(caching[url]);
        }

        return exists;
    }

    static fetch(data, done, callback) {
        if(this.fromCache(data, callback)) {
            return;
        }

        const { url } = data;


        let responseData = {};
        // console.log("Fetch:", data.method + ' : ' + data.url);

        fetch(url, this.buildFetchOptions(data))
        .then(response => {
            const { status, statusText } = response;
            responseData = { status, statusText }

            return response.json();
        })
        .then(json => {
            if(json == undefined) {
                return done(new Error("Response is undefined."));
            }

            responseData.json = json;
            
            caching[url] = responseData;
            global.lastResultData = json;
            // console.log(json);

            callback(responseData);
        })
        .catch(err => done(new Error(err)));
    }
}