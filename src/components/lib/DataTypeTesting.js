"use strict";

var Utils = require ('../Utils');

module.exports = class DataTypeTesting {
    constructor(id) {
        this.id = id;
    }

    isOfType(key, typeString) {
        const title = `IsOfType - response key "${key}" is "${typeString}"`;
        const data = Utils.getData(this.id);
        
        it(title, done => {
            if(!key) {
                return done(new Error("No key specified."));
            }

            Utils.fetch(data, done, response => {
                const { json } = response;
                const jsonKeyToAnalyse = Utils.getJsonKeyToAnalyse(key, json);
                
                if(jsonKeyToAnalyse == undefined) {
                    return done(new Error(`Key "${key}" doesn't exist.`));
                }

                const type = typeof jsonKeyToAnalyse;
                if(type !== typeString) {
                    return done(new Error(`Value for key "${key}" is not a ${typeString} value (type = "${type}").`));
                }

                done();
            });
        });

        return this;
    }

    isBoolean(key) {
        this.isOfType(key, 'boolean');
        return this;
    }

    isString(key) {
        this.isOfType(key, 'string');
        return this;
    }

    isNumber(key) {
        this.isOfType(key, 'number');
        return this;
    }

    isObject(key) {
        this.isOfType(key, 'object');
        return this;
    }

    isNotOfType(key, typeString) {
        const title = `IsNotOfType - response key "${key}" is not "${typeString}"`;
        const data = Utils.getData(this.id);
    
        it(title, done => {
            if(!key) {
                return done(new Error("No key specified."));
            }
    
            Utils.fetch(data, done, response => {
                const { json } = response;
                const jsonKeyToAnalyse = Utils.getJsonKeyToAnalyse(key, json);
                
                if(jsonKeyToAnalyse == undefined) {
                    return done(new Error(`Key "${key}" doesn't exist.`));
                }
    
                const type = typeof jsonKeyToAnalyse;
                if(type !== typeString) {
                    return done();
                }
    
                done(new Error(`Value for key "${key}" is not a ${typeString} value (type = "${type}").`));
            });
        });
    
        return this;
    }
    
    isNotBoolean(key) {
        this.isNotOfType(key, 'boolean');
        return this;
    }
    
    isNotString(key) {
        this.isNotOfType(key, 'string');
        return this;
    }
    
    isNotNumber(key) {
        this.isNotOfType(key, 'number');
        return this;
    }
    
    isNotObject(key) {
        this.isNotOfType(key, 'object');
        return this;
    }
}