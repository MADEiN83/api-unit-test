"use strict";

var Utils = require('../Utils');
var KeyValueTesting = require('./KeyValueTesting')

module.exports = class DefaultTesting extends KeyValueTesting {
    constructor(data) {
        super(data.id);
        this.id = data.id;
        Utils.setData(data);
    }

    noCache() {
        const data = Utils.getData(this.id);
        data.noCache = true;
        Utils.setData(data);

        return this;
    }

    hasStatus(status) {
        const title = `HasStatus - Status: "${status}"`;
        const data = Utils.getData(this.id);

        it(title, function(done) {
            if(!status) {
                return done(new Error("No status specified."));
            }

            Utils.fetch(data, done, response => {
                const { statusText } = response;

                if(statusText != status) {
                    return done(new Error(`Status (${statusText}) is not equals to "${status}".`));
                }

                done();
            })
        });

        return this;
    }

    hasStatusCode(statusCode) {
        const title = `HasStatusCode - Status code: ${statusCode}`;
        const data = Utils.getData(this.id);

        it(title, function(done) {
            if(!statusCode) {
                return done(new Error("No status code specified."));
            }

            Utils.fetch(data, done, response => {
                const { status } = response;

                if(status != statusCode) {
                    return done(new Error(`Status code (${status}) is not equals to "${statusCode}".`));
                }

                done();
            })
        });

        return this;
    }
}