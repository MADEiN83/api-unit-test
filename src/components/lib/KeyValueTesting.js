"use strict";

var Utils = require ('../Utils');
var DataTypeTesting = require('./DataTypeTesting')

module.exports = class KeyValueTesting extends DataTypeTesting {
    constructor(id) {
        super(id);
        this.id = id;
    }
    
    hasKey(key) {
        const title = `HasKey - key "${key}" exists`;
        const data = Utils.getData(this.id);
        
        it(title, function(done) {
            if(!key) {
                return done(new Error("No key specified."));
            }

            Utils.fetch(data, done, response => {
                const { json } = response;
                const jsonKeyToAnalyse = Utils.getJsonKeyToAnalyse(key, json);

                if(jsonKeyToAnalyse == undefined) {
                    return done(new Error(`Key "${key}" doesn't exist.`));
                }

                done();
            })
        });

        return this;
    }

    hasKeys(...keys) {
        for(let key of keys) {
            this.hasKey(key);
        }
        return this;
    }

    hasNoKey(key) {
        const title = `HasNoKey - key "${key}" doesn't exist`;
        const data = Utils.getData(this.id);

        it(title, function(done) {
            if(!key) {
                return done(new Error("No key specified."));
            }

            Utils.fetch(data, done, response => {
                const { json } = response;
                const jsonKeyToAnalyse = Utils.getJsonKeyToAnalyse(key, json);

                if(jsonKeyToAnalyse == undefined) {
                    return done();
                }

                done(new Error(`Key "${key}" exists.`));
            })
        });

        return this;
    }
    
    hasNoKeys(...keys) {
        for(let key of keys) {
            this.hasNoKey(key);
        }
        return this;
    }

    match(key, value) {
        const title = `Match - key "${key}" equals to "${value}"`;
        const data = Utils.getData(this.id);
        
        it(title, function(done) {
            if(!key) {
                return done(new Error("No key specified."));
            }

            Utils.fetch(data, done, response => {
                const { json } = response;
                const jsonKeyToAnalyse = Utils.getJsonKeyToAnalyse(key, json);

                if(jsonKeyToAnalyse == undefined) {
                    return done(new Error(`Key "${key}" doesn't exist.`));
                }

                if(jsonKeyToAnalyse != value) {
                    return done(new Error(`The value for key "${key}" was "${jsonKeyToAnalyse}". The expected value is "${value}"`));
                }

                done();
            })
        });

        return this;
    }

    matchOne(key, values) {
        const stringValues = values.join(', ');
        const title = `MatchOne - value for key "${key}" match with one of: ${stringValues}`;
        const data = Utils.getData(this.id);

        it(title, function(done) {
            if(!key) {
                return done(new Error("No key specified."));
            }

            Utils.fetch(data, done, response => {
                const { json } = response;
                const jsonKeyToAnalyse = Utils.getJsonKeyToAnalyse(key, json);

                if(jsonKeyToAnalyse == undefined) {
                    return done(new Error(`Key "${key}" doesn't exist.`));
                }

                for(let value of values) {
                    if(jsonKeyToAnalyse === value) {
                        return done();
                    }
                }

                return done(new Error(`The value for the key ${key} is not one of: ${stringValues}`));
            })
        });

        return this;
    }

    contains(key, value) {
        const title = `Contains - response key "${key}" contains "${value}"`;
        const data = Utils.getData(this.id);

        it(title, done => {
            if(!key) {
                return done(new Error("No key specified."));
            }

            Utils.fetch(data, done, response => {
                const { json } = response;
                const jsonKeyToAnalyse = Utils.getJsonKeyToAnalyse(key, json);

                if(jsonKeyToAnalyse == undefined) {
                    return done(new Error(`Key "${key}" doesn't exist.`));
                }

                if(jsonKeyToAnalyse.indexOf(value) < 0) {
                    return done(new Error(`Value for key "${key}" doesn't contain "${value}".`));
                }

                done();
            });
        });

        return this;
    }

    containsOne(key, values) {
        const stringValues = values.join(', ');
        const title = `ContainsOne - value for key "${key}" contains at least one of: ${stringValues}`;
        const data = Utils.getData(this.id);

        it(title, done => {
            if(!key) {
                return done(new Error("No key specified."));
            }

            Utils.fetch(data, done, response => {
                const { json } = response;
                const jsonKeyToAnalyse = Utils.getJsonKeyToAnalyse(key, json);

                if(jsonKeyToAnalyse == undefined) {
                    return done(new Error(`Key "${key}" doesn't exist.`));
                }

                for(let value of values) {
                    if(jsonKeyToAnalyse.indexOf(value) >= 0) {
                        return done();
                    }
                }

                return done(new Error(`The value for the key ${key} doesn't contain one of: ${stringValues}`));
            });
        });

        return this;
    }
}