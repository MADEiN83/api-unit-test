"use strict";

var expect = require('chai').expect;
var HttpsProxyAgent = require('https-proxy-agent');
var DefaultTesting = require('./lib/DefaultTesting');

module.exports = class ApiUnitTest {
    static setOptions(options) {
        const { proxy, baseApiUrl } = options;

        const proxyString = 'http://' + proxy.username 
            + ':' + proxy.pwd 
            + '@' + proxy.url 
            + ':' + proxy.port;

        this.appProxy = new HttpsProxyAgent(proxyString);
        this.apiUrl = baseApiUrl;
    }
 
    static get(endpoint, formData, header) {
        const url = this.apiUrl + endpoint;
        const ut = this.call(url, 'GET', formData, header);
        
        return ut;
    }
 
    static post(endpoint, formData, header) {
        const url = this.apiUrl + endpoint;
        const ut = this.call(url, 'POST', formData, header);

        return ut;
    }

    static call(url, method, formData, header) {
        const child = new DefaultTesting({
            id: guid(),
            url,
            method,
            formData,
            header,
            proxy: this.appProxy,
        });

        return child;
    }
}

function guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}