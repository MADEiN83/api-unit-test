# api-unit-test [![Build Status](https://travis-ci.org/MADEiN83/api-unit-test.svg?branch=master)](https://travis-ci.org/MADEiN83/api-unit-test) [![Maintainability](https://api.codeclimate.com/v1/badges/2eb2096632386f553d0a/maintainability)](https://codeclimate.com/github/MADEiN83/api-unit-test/maintainability) [![npm version](https://badge.fury.io/js/api-unit-test.svg)](https://badge.fury.io/js/api-unit-test)

NodeJS module that creates API unit test

## Installation
```bash
npm i api-unit-test chai mocha https-proxy-agent node-fetch -D
# Sets packages scripts 'test' to: mocha --reporter spec -t [TIMEOUT_IN_MS], then:

npm test
```

See a test result:
```bash
 /GET [your endpoint]
    √ HasKey - key "message" exists
    √ HasKey - key "success" exists
    √ HasStatusCode - Status code: 403
    √ HasStatus - Status: "Forbidden"
    √ HasNoKey - key "test" doesn't exist
    √ HasNoKey - key "test_2" doesn't exist
    √ HasNoKey - key "test_3" doesn't exist
    √ Match - key "success" equals to "false"
    √ Match - key "message" equals to "No token provided."
    √ MatchOne - value for key "message" match with one of: nothing, No token provided.
    √ Contains - response key "message" contains "token"
    √ ContainsOne - value for key "message" contains at least one of: nothing, token
    √ IsOfType - response key "message" is "string"
    √ IsOfType - response key "success" is "boolean"
    √ IsOfType - response key "message" is "string"
    √ IsNotOfType - response key "message" is not "number"
    √ IsNotOfType - response key "message" is not "boolean"
    √ IsNotOfType - response key "success" is not "string"

    19 passing (32ms)
```

## Configuration
| Name | Required | Description
| -- | -- | --
| ```proxy``` | ```false``` | Sets a proxy with ```url```, ```port```, ```username``` and ```pwd```
| ```baseApiUrl``` | ```true``` | Sets the default API route
```js
const ApiUnitTest = require('api-unit-test');
const options = {
    proxy: {
        url:'http://proxy.com',
        port:'8080',
        username:'me',
        pwd:'123123'
    },
    baseApiUrl: 'https://website.com/v1/',
};

ApiUnitTest.setOptions(options);
```

## Features
### hasKey([key])
```js
ApiUnitTest
    .get("[API URL]")
    .hasKey("message")
```

### hasKeys([... keys])
```js
ApiUnitTest
    .get("[API URL]")
    .hasKeys("message", "success")
```

### hasStatusCode([statusCode])
```js
ApiUnitTest
    .get("[API URL]")
    .hasStatusCode(403)
```

### hasStatus([string])
```js
ApiUnitTest
    .get("[API URL]")
    .hasStatus("Forbidden")
```

### hasNoKey([string])
```js
ApiUnitTest
    .get("[API URL]")
    .hasNoKey("salut_toi")
```

### hasNoKeys([... strings])
```js
ApiUnitTest
    .get("[API URL]")
    .hasNoKeys("fzef", "gergzrgrg")
```

### match([key], [value])
```js
ApiUnitTest
    .get("[API URL]")
    .match("success", false)
```

### matchOne([key], [... values])
```js
ApiUnitTest
    .get("[API URL]")
    .matchOne("message", ["nothing", "No token provided."])
```

### contains([key], [value])
```js
ApiUnitTest
    .get("[API URL]")
    .contains("message", "token")
```

### containsOne([key], [... values])
```js
ApiUnitTest
    .get("[API URL]")
    .containsOne("message", ["nothing", "token"])
```

### isOfType([key], [type])
```js
ApiUnitTest
    .get("[API URL]")
    .isOfType("message", "string")
```

### isBoolean([key])
```js
ApiUnitTest
    .get("[API URL]")
    .isBoolean("success")
```

### isString([key])
```js
ApiUnitTest
    .get("[API URL]")
    .isString("message")
```

### isNumber([key])
```js
ApiUnitTest
    .get("[API URL]")
    .isNumber("test")
```

### isObject([key])
```js
ApiUnitTest
    .get("[API URL]")
    .isObject("test")
```

### isNotOfType([key], [type])
```js
ApiUnitTest
    .get("[API URL]")
    .isNotOfType("message", "number")
```

### isNotBoolean([key])
```js
ApiUnitTest
    .get("[API URL]")
    .isNotBoolean("message")
```

### isNotString([key])
```js
ApiUnitTest
    .get("[API URL]")
    .isNotString("success")
```

### isNotNumber([key])
```js
ApiUnitTest
    .get("[API URL]")
    .isNotNumber("test")
```

### isNotObject([key])
```js
ApiUnitTest
    .get("[API URL]")
    .isNotObject("test")
```
